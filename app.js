'use strict'
var express = require('express');
var bodyparse = require('body-parser');
var schedule = require('node-schedule');
var trabajoscron = require('./servicios/cron'); 
var app = express();

// No es parte de la conf de express, es uso de la libreria node-schedule para cron jobs
// TODOS LOS DIAS PRIMERA HORA
var vigilante = schedule.scheduleJob('0 1 * * *', function(){
    trabajoscron.actualizarStatusCurso();
    trabajoscron.calificarCurso();
  });

//cargamos vistas
app.use(express.static('public'));
// app.use(express.static(path.join(__dirname + 'public')));


//section 1 : importacion rutas
var rutas_docente = require('./rutas/docente');
var rutas_curso = require('./rutas/curso');
var rutas_estudiante = require('./rutas/estudiante');
var rutas_comentarios = require('./rutas/comentario');
var rutas_etiqueta = require('./rutas/etiqueta');

//section 2: cargar middlewares
app.use(bodyparse.urlencoded({extended: false}));
app.use(bodyparse.json());

//section 3: cors
app.use((req, res, next)=>{
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', '*');
    res.header('Access-Control-Allow-Methods', 'GET ,POST ,PUT, OPTIONS, PUT ,DELETE');
    res.header('Allow', 'GET ,POST ,PUT, OPTIONS, PUT ,DELETE');
    next();
});

//section 4: carga de rutas
app.use('/api', rutas_docente);
app.use('/api', rutas_curso);
app.use('/api', rutas_estudiante);
app.use('/api', rutas_comentarios);
app.use('/api', rutas_etiqueta);

app.use('/imagenes', express.static('imagenes', {redirect: false}));




//section 5: exportacion de app
module.exports= app;
