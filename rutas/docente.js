'use strict'
 var express = require('express');
 var ControladorDocente = require('../controladores/docente')
 var api = express.Router();
 var auth = require('../servicios/jwt_decode');

 api.get('/home', ControladorDocente.home);
 api.post('/insert', ControladorDocente.insert);
 api.post('/crearDocente', ControladorDocente.crearDocente);
 api.get('/obtenerDocente/:id', ControladorDocente.obtenerDocente);
 api.get('/obtenerDocentes/:page?/:itemPerPage?', ControladorDocente.obtenerDocentes);
 api.post('/login',ControladorDocente.login);
 api.put('/actualizarDocente/:id',auth.decode ,ControladorDocente.actualizarDocente)

 module.exports = api;
