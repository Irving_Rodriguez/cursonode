'use strict'
 var express = require('express');
 var ControladorCurso = require('../controladores/curso')
 var api = express.Router();
 var auth = require('../servicios/jwt_decode');
 var multipart = require('connect-multiparty');
 var path_image = multipart({uploadDir: './imagenes'})

 api.post('/crearCurso', auth.decode, ControladorCurso.crearCurso);
 api.get('/obtenerCurso/:id', ControladorCurso.obtenerCurso);
 api.get('/obtenerCursosDisponibles/:page?/:itemPerPage?', ControladorCurso.obtenerCursosDisponibles);
api.post('/subirImagen/:id',[auth.decode, path_image],ControladorCurso.subirImagen);
api.put('/actualizarCurso/:id',auth.decode ,ControladorCurso.actualizarCurso)

 module.exports = api;