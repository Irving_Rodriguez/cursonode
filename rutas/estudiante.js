'use strict'
 var express = require('express');
 var ControladorEstudiante = require('../controladores/estudiante')
 var api = express.Router();
 var auth = require('../servicios/jwt_decode');

api.post('/crearEstudiante',ControladorEstudiante.crearEstudiante);
api.post('/verificarEstudiante', ControladorEstudiante.verificarEstudiante);

 module.exports = api;