'use strict'
var express = require('express');
var ControladorComentario = require('../controladores/comentario');
var api = express.Router();
var md_auth = require('../servicios/jwt_decode');

api.post('/crearComentarioCurso', ControladorComentario.crearComentarioCurso);
api.post('/crearComentarioDocente', ControladorComentario.crearComentarioDocente)
api.get('/obtenerComentariosCurso/:receptor_curso',ControladorComentario.obtenerComentariosCurso);
api.get('/obtenerComentariosDocente/:receptor_docente',ControladorComentario.obtenerComentariosDocente);

module.exports = api;