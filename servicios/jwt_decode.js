'use strict'
var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'curso node clave secreta';

exports.decode = function(req, res, next){
    console.log
    if(!req.headers.authorization){
        res.status(403).send({message:'La peticion no tiene las cabeceras de autorizacion'});
    } else{
        var token = req.headers.authorization.replace(/['"]+/g,'');
        try{
            var payload = jwt.decode(token,secret);
            //console.log(payload.exp,  '<=' , moment().unix())
            //if(payload.exp <= moment().unix()){
             //   res.status(401).send({message:'El token a expirado'});
           // }
        } catch(ex){
            console.log(ex)
            res.status(401).send({message:'el token no es valido', error: String(ex)});
        }
        req.docente = payload;
        next();
    }
}