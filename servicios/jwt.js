'use strict'
var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'curso node clave secreta';
exports.auth = function(docente){
    var payload = {
        sub: docente._id,
        nombre: docente.nombre,
        correo: docente.correo,
        iat: moment().unix(),  // fecha creacion
        exp: moment().add(20,'days').unix()   //fecha expiracion
    }
    //.add(7, 'days')
    //.add(30,'seconds')

    return jwt.encode(payload, secret)
}