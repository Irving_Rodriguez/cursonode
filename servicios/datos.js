var Estudiante = require('../modelos/estudiante');
var Curso = require('../modelos/curso');
var Correos = require('../controladores/correos')
var Docente = require('../modelos/docente');

async function  actualizarRegistros(cursoId, estudiante){
    let update_docentes = {};
    let update_cursos = {};
    var curso = await Curso.findById({_id: cursoId})
    if( curso['cupoLimite'] == 0){
        update_cursos = {
                        $set:{ cupoLimite: 0, status: 3}
                        }
    } else{
        update_cursos = { 
                        $inc: { cupoLimite: -1 },
                        $push: {registrados: estudiante._id}
                        }
        update_docentes = { 
                        $inc: {total_estudiantes: 1}
                        }
        //send email 
        await Correos.inscripcionCurso(curso,estudiante)
        await Docente.findByIdAndUpdate({_id: curso.docente}, update_docentes,{upsert: true});
    }
    await Curso.findByIdAndUpdate({_id:cursoId}, update_cursos,{upsert: true});

}

async function actualizarRating(comentario){
   const update_status = {$set: {'status_calificacion': true}}  
   await  Estudiante.findOneAndUpdate({_id: comentario.emisor_estudiante}, update_status,{upsert: true})

    let curso = await  Curso.findOne({_id: comentario.receptor_curso});
       let total=0 , calificaciones = curso.valoracion.listRaiting , length = 1;
       if(calificaciones){
        calificaciones.forEach((a)=>{total += a;});
        length = calificaciones.length +1;
       }
        const update_raiting = {
            $push: {"valoracion.listRaiting": comentario.calificacion},
            $set: {"valoracion.rating": (total+comentario.calificacion)/length}
        };

        
        await Curso.updateOne({_id: curso._id}, update_raiting,{upsert: true});

}

function formatoFecha(fecha, today){
    if(today){
        return moment().format('YYYY-MM-DD')
    } else {
        return moment(fecha).format('DD/MM/YYYY')
    }
  
}


module.exports = {
    actualizarRegistros,
    actualizarRating,
    formatoFecha
}