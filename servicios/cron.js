'use strict'

// Cargamos los modelos para usarlos posteriormente
var Curso = require('../modelos/curso');
var Correos = require('../controladores/correos');
var Servicios = require('../servicios/datos');
var moment = require('moment');

async function actualizarStatusCurso(){
  let hoy = Servicios.formatoFecha('',true);
  let fecha_hoy = new Date(hoy)
  Curso.find({$and:[{fecha_inicio:{"$gte": fecha_hoy}},{fecha_final:{"$lt": fecha_hoy}}]}).populate('docente').populate('registrados').exec( async function 
    (err, cursos){
      cursos.forEach((curso)=>{
        for (let estudiante of curso['registrados']){
          Correos.inicioCursos(curso,estudiante['correo'])
        }
         Curso.findOneAndUpdate({_id: curso['_id']}, {$set: {'status': 4}}) 
      })
  })
}

async function calificarCurso(){
  let hoy =  Servicios.formatoFecha('',true);
  let fecha_hoy = new Date(hoy)
  Curso.find({$and:[{fecha_inicio:{"$gte": fecha_hoy}},{fecha_final:{"$lt": fecha_hoy}}]}).populate('docente').populate('registrados').exec(async function 
    (err, cursos){
      cursos.forEach((curso)=>{
        for (let estudiante of curso['registrados']){
          Correos.calificarCurso(curso,estudiante)
        }
         Curso.findOneAndUpdate({_id: curso['_id']}, {$set: {'status': 5}});
      })
  })
}
//Subir archivos solo usar Autorization con token ,remover apllication/json y mandarlo en formdata con clave archivo

    module.exports = {
       actualizarStatusCurso,
       calificarCurso



      };