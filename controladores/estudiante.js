var Estudiante = require('../modelos/estudiante');
var Curso = require('../modelos/curso');
var fs = require('fs');
var path = require('path');
var config = require('../configuracion/config');
var bcrypt = require('bcrypt-nodejs')
var servicios = require('../servicios/datos');

function crearEstudiante(req, res){
    var params = req.body;
    var estudiante = new Estudiante(params)
    //status_calificacion
    //verificar duplicado
    Estudiante.find({$and:[{correo:params.correo},{curso: params.curso}]},(err, verificarDuplicado)=>{
        if(err)  return res.status(500).send({message:'error al crear estudiante', status: false, err: String(err)})

        if(verificarDuplicado && verificarDuplicado.length > 0){
            res.status(200).send({message:'Ya esta inscrito al curso', status: false})
        } 
        bcrypt.hash(params.correo+ params.curso , null, null, (err, hash)=>{
            if(err) return  res.status(500).send({message:'error al crear estudiante', status: false, err: String(err)})
            estudiante.token_calificacion = hash;
            estudiante.status_calificacion = false;
            estudiante.save(async(err, estudianteRegistrado)=>{
                if(err)  res.status(500).send({message:'error al crear estudiante', status: false, err: String(err)})
                 // actualizar status , agregar registrados,
                 await servicios.actualizarRegistros(params.curso, estudianteRegistrado);
                 res.status(200).send({estudiante: estudianteRegistrado, status: true})

            })
        })

    })
    
}

function verificarEstudiante(req, res){
    var params = req.body;
    var token_calificacion = params.token_calificacion;
    
    Estudiante.findOne({token_calificacion: token_calificacion}, (err, estudiante) => {
        if(err) return res.status(500).send({mensaje:'error al verificar al estudiante', status: false });

        if(estudiante && estudiante.status_calificacion == false){
                              
            return res.status(200).send({estudiante: estudiante ,status: true});
               
        } else {
			return res.status(404).send({mensaje: 'Las credenciales no coinciden', status: false});
		}

    })
}

module.exports = {
    crearEstudiante,
    verificarEstudiante
}