
var fs = require('fs');
var configuracion = require('../Configuracion/config');
const nodemailer = require("nodemailer");
const { google } = require("googleapis");
const OAuth2 = google.auth.OAuth2;

const authData = {
    managerEmailer:{
        user: 'cursonodesoporte@gmail.com',
        ClientID: "309179029994-9ebf9r37t95lkdq3h5tkm6u2j4tt015u.apps.googleusercontent.com", // ClientID
        Secret: "7BUp3ZtvUnaBCQGWoxPTit1p", // Client Secret
        URL: "https://developers.google.com/oauthplayground", // Redirect URL};,
        refresh_token: "1//04PqrPkZvypTkCgYIARAAGAQSNwF-L9IrMq-bHh3wfihw8pMrxDtgjMrOkx4BMnU82hRhyWLO7g0ArVaQDGtVVRHFL56-0WcgT_k"
    }
    };

async function setUpCredentialsAndTransporter(){
    // Generate test SMTP service account from ethereal.email
// Only needed if you don't have a real mail account for testing
    const oauth2Client = new OAuth2(
        authData.managerEmailer.ClientID, authData.managerEmailer.Secret, authData.managerEmailer.URL
    );
    oauth2Client.setCredentials({
        refresh_token: authData.managerEmailer.refresh_token
    });
    const tokens = await oauth2Client.getRequestHeaders();
    const accessToken =  tokens.Authorization.split(' ')[1];
    return nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: authData.managerEmailer.user,
            type: "OAuth2",
            clientId: authData.managerEmailer.ClientID,
            clientSecret: authData.managerEmailer.Secret,
            refreshToken: authData.managerEmailer.refresh_token,
            accessToken: accessToken
        },
        tls: {rejectUnauthorized: false}
    });
}


async function inscripcionCurso(curso,estudiante){
    var htmlEmail = fs.readFileSync('./plantillas/incripcionCurso.html');
    htmlEmail = htmlEmail.toString().replace(/1shm/g, curso.fecha_inicio).replace(/2shm/g,curso.fecha_final).
    replace(/3shm/g, curso.nombre).replace(/4shm/g, estudiante.nombre)
    const transporter = await setUpCredentialsAndTransporter();
        // setup email data with unicode symbols
        let mailOptions = {
            from: 'NODE EXPRESS MONGO <'+authData.user+'>', // sender address
            to: estudiante.correo, // list of receivers
            subject: '¡Bienvenido a CURSO NODE!', // Subject line
            text: 'Cursos',
            html: htmlEmail.toString()
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log(error);
                
                transporter.close();
            }else {
                // return res.json({status: 1, result:'email sent'});
                console.log('Enviado')
            }
        });
}

async function inicioCursos(curso,emailEstudiante){

    var htmlEmail = fs.readFileSync('./plantillas/inicioCurso.html');
    htmlEmail = htmlEmail.toString().replace(/1shm/g, curso.fecha_inicio).replace(/2shm/g, fecha_final)
    .replace(/3shm/g, curso.nombre).replace(/4shm/g, curso.docente.nombre)
    const transporter = await setUpCredentialsAndTransporter();
        // setup email data with unicode symbols
        let mailOptions = {
            from: 'INICIO CURSO <'+authData.user+'>', // sender address
            to: emailEstudiante, // list of receivers
            subject: '¡Notificación!', // Subject line
            text: 'Instructor '+ curso.docente.nombre,
            html: htmlEmail.toString()
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log(error);
                // return res.json({status: 0, result:'error sent: ' + error});
                transporter.close();
            }else {
                // return res.json({status: 1, result:'email sent'});
                console.log('Enviado')
            }
        });
}

async function calificarCurso(curso, estudiante){
    var url = configuracion.host + 'curso/calificar/' + estudiante.token_calificacion;
    var htmlEmail = fs.readFileSync('./plantillas/finalCurso.html');
    htmlEmail = htmlEmail.toString().replace(/1shm/g, url)
    replace(/2shm/g, curso.nombre).replace(/3shm/g, estudiante.nombre)
    const transporter = await setUpCredentialsAndTransporter();
        // setup email data with unicode symbols
        let mailOptions = {
            from: 'FIN CURSO <'+authData.user+'>', // sender address
         
            to: estudiante.correo, // list of receivers

            subject: '¡Notificación!', // Subject line
            text: 'Instructor '+ curso.docente.nombre, 
            html: htmlEmail.toString()
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log(error);
                // return res.json({status: 0, result:'error sent: ' + error});
                transporter.close();
            }else {
                // return res.json({status: 1, result:'email sent'});
                console.log('Enviado')
            }
        });

}
module.exports = {
    inscripcionCurso,
    inicioCursos,
    calificarCurso

   };