'use strict'
// Cargamos los modelos para usarlos posteriormente
var Comentario = require('../modelos/comentario');
var Estudiante = require('../modelos/estudiante');
var Servicios = require('../servicios/datos');
var mongoosePaginate = require('mongoose-pagination');


function crearComentarioCurso(req,res){
    var params = req.body;
    var comentario = new Comentario(params);
    comentario.tipo = 'curso';

    comentario.save(async(err, comentarioRegistrado)=>{
        if (err) return res.status(500).send({mensaje:'Error al guardar el comentario' , status: false, err: String(err)})
        if(comentarioRegistrado){
    
                await Servicios.actualizarRating(comentarioRegistrado);
                res.status(200).send({comentario: comentarioRegistrado, status: true});
          
        } else {
            res.status(404).send({mensaje:'No se pudo registrar la calificacion' , status: false});
        }   
    });
}

function crearComentarioDocente(req,res){
    var params = req.body;
    var comentario = new Comentario(params);
    comentario.tipo = 'docente';
    comentario.save((err, comentarioRegistrado)=>{
        if (err) return res.status(500).send({mensaje:'error crear comentario' , status: false, err:String(err)})
        //si el callback de la insercion retorna datos correctos lo devolvemos con status 200
        if(comentarioRegistrado){
            res.status(200).send({comentario: comentarioRegistrado, status: true});
        } else {
            res.status(404).send({mensaje:'No se pudo registrar su reseña' , status: false});
        }   
    });
}

function obtenerComentariosCurso(req,res) {
    var cursoId = req.params.receptor_curso;
    if(cursoId) {
        Comentario.find({receptor_curso: cursoId},(err, comentarios) => {
            if(err) return res.status(500).send({message: 'Error en la petición', status: false});

            if(!comentarios) return res.status(404).send({message: 'No hay comentarios disponibles', status: false});
            
            return res.status(200).send({comentarios: comentarios, status: true});

        });	
    }else{

       return res.status(404).send({message: 'No hay comentarios disponibles', status: false});
    }

}

function obtenerComentariosDocente(req,res) {
    var docenteId = req.params.receptor_docente;
    if(docenteId) {
        Comentario.find({receptor_docente: docenteId},(err, comentarios) => {
            if(err) return res.status(500).send({message: 'Error en la petición', status: false});

            if(!comentarios) return res.status(404).send({message: 'No hay comentarios disponibles', status: false});
            
            return res.status(200).send({comentarios: comentarios, status: true});

        });	
    }else{

       return res.status(404).send({message: 'No hay comentarios disponibles', status: false});
    }

}



module.exports = {
       
    crearComentarioCurso,
    crearComentarioDocente,
    obtenerComentariosCurso,
    obtenerComentariosDocente
};