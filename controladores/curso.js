var Curso = require('../modelos/curso');
var Etiquetas = require('../modelos/etiqueta');
var fs = require('fs');
var path = require('path');
var config = require('../configuracion/config');


function crearCurso(req, res){
    var params = req.body;
    var curso = new Curso(params)
    curso.docente = req.docente.sub;

    curso.save((err, cursoRegistrado)=>{
        if(err) return res.status(500).send({mensaje: 'Error en insertar un curso',status: false, err});
        res.status(200).send({Curso: cursoRegistrado, status: true});
    });
}

function obtenerCursosDisponibles(req,res){

    var params = req.params;
    var page = 1;
    if(params.page){
        page = parseInt(params.page);
    }
    var itemPerPage = 5;
    if(params.itemPerPage){
        itemPerPage = parseInt(params.itemPerPage);
    }

    Curso.find({status :{$ne: 5}}).paginate(page, itemPerPage,(err, cursos, total)=>{
       console.log(cursos)
       if(err) res.status(500).send({message:'Error', status: false})
           return res.status(200).send({
               cursos,
               total,
               page,
               itemPerPage,
               pages: Math.ceil(total/itemPerPage)
            })
   })
}

async function obtenerCurso(req,res){
    var params = req.params;
    const etiquetas = await Etiquetas.find({referenciaId: params.id} , {etiqueta: 1}); 
     Curso.find({_id: params.id}).
     populate({path: 'registrados'}).
     populate({path:'docente'}).
     exec((err, curso)=>{
        console.log(curso)
        if(err) res.status(500).send({message:'Error', status: false})
            return res.status(200).send({curso,etiquetas})
    })
}

function subirImagen(req,res){
    console.log(req.files)
    var cursoId= req.params.id;
    if(req.files){
        var direcccionArchivo  = req.files.archivo.path;
        var extencion = req.files.archivo.name.split('.')[1]
        var nombreArchivo = cursoId + '.' + extencion;
        //old path
        const old_path = path.join(__dirname,'../',direcccionArchivo);
        console.log(old_path)
        const new_path = path.join(__dirname,'../imagenes/', nombreArchivo);
        console.log(new_path);
        fs.renameSync(old_path,new_path);

        if(extencion =='png'){
            
            Curso.findByIdAndUpdate({_id:cursoId},{image: config.host + '/imagenes/'+nombreArchivo},(err, imagenActualizada)=>{
                if(err) return  res.status(500).send({message:'El no pudo ser actualizado', status: false})
                res.status(200).send({message: imagenActualizada, status: true})
            })

        } else{
            fs.unlink(new_path,(err)=>{
                if(err) return  res.status(500).send({message:'El no pudo ser actualizado', status: false})
                return res.status(200).send({message:'El archivo no tienela extencion requerida', status: false})
            })
           
        }

    }else{
        res.status(200).send({message:'El archivo es requerido', status: false})
    }
  
}

function actualizarCurso(req,res){
    var cursoId = req.params.id;
    var update = req.body;
 
    Curso.findOneAndUpdate({_id: cursoId},update,{new:true},(err, cursoActualizado)=>{
        if(err) return res.status(500).send({message:'Error', status: false})

        res.status(200).send({cursoActualizado, status: true})

    })

}


module.exports ={
    crearCurso,
    obtenerCursosDisponibles,
    obtenerCurso,
    subirImagen,
    actualizarCurso
}