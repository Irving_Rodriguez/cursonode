'use strict'

var ModelDocente = require('../modelos/docente');
var bcrypt = require('bcrypt-nodejs');
var mongoosePaginate = require('mongoose-pagination');
var  servicios = require('../servicios/jwt')


function home(req,res){
    //console.log({mensaje: 'Hola mundo'});
    res.status(200).send({mensaje: 'Hola mundo'})
};

function insert(req,res){
    console.log(req.body.nombre)
    res.status(200).send({mensaje: 'datos enviados correctamente'})
};

function crearDocente(req, res){
    var params = req.body;
    var Docente = new ModelDocente();

    Docente.nombre = params.nombre;
    Docente.cargo = params.cargo;
    Docente.resumen =  params.resumen;
    Docente.total_estudiantes = 0;
    Docente.imagen_perfil = params.imagen_perfil;
    Docente.correo = params.correo;
    Docente.password = params.password;
    Docente.redes_sociales.facebook = params.facebook;
    Docente.redes_sociales.youtube = params.youtube;
    Docente.redes_sociales.twitter = params.twitter;
    Docente.redes_sociales.linkedin = params.linkedin;

    ModelDocente.find({correo: params.correo},(err, duplicado)=>{
        if(err) res.status(500).send({mensaje:  err, status: false});
        
        if(duplicado && duplicado.length >=1){
            res.status(200).send({mensaje: 'Docente ya existe',status: false});
        } else {
            bcrypt.hash(params.password, null, null,(err,hash)=>{
                if(err) res.status(500).send({mensaje: 'Error en bcrypt',status: false});
                Docente.password = hash
                Docente.save((err, docenteRegistrado)=>{
                    if(err) res.status(500).send({mensaje: 'Error en insertar docente',status: false});
                    res.status(200).send({docente: docenteRegistrado, status: true});
                });
                
            })
        }
    })

}

 async function obtenerDocente(req,res){
    var params = req.params;
    const etiquetas = await Etiquetas.find({referenciaId: params.id},{etiqueta: 1}); 
     ModelDocente.find({_id: params.id},{password:0},(err, docente)=>{
        console.log(docente)
        if(err) res.status(500).send({message:'Error', status: false})
            return res.status(200).send({docente,etiquetas})
    })
}

function obtenerDocentes(req,res){
    var params = req.params;
    var page = 1;
    if(params.page){
        page = parseInt(params.page);
    }
    var itemPerPage = 5;
    if(params.itemPerPage){
        itemPerPage = parseInt(params.itemPerPage);
    }

    ModelDocente.find({},{password:0}).paginate(page, itemPerPage,(err, docentes, total)=>{
       console.log(docentes)
       if(err) res.status(500).send({message:'Error', status: false})
           return res.status(200).send({
               docentes,
               total,
               page,
               itemPerPage,
               pages: Math.ceil(total/itemPerPage)
            })
   })
}

function login(req,res){
    var params = req.body;
    console.log(params)
    var p_password = params.password;
    var p_correo = params.correo;

    var p_gettoken = false;
    if(params.gettoken){
         p_gettoken = params.gettoken
    }
    

    //buscar al docente
    ModelDocente.findOne({correo: p_correo},(err, docente)=>{
        if(err) res.status(500).send({message:'Error', status: false})
        if(docente){
            bcrypt.compare(p_password,docente.password,(err, verificado)=>{
                //crear token validacion
                if(err) return res.status(500).send({message:'as credenciales no coinciden', status: false})
                if(verificado){
                    docente.password = undefined;
                    if(params.gettoken){
                        return res.status(200).send(servicios.auth(docente))
                    }else{
                        return res.status(200).send({docente})
                    }  
                } else {
                    return res.status(404).send({message:'as credenciales no coinciden', status: false})
                }
            })
        }else{
            return res.status(404).send({message:'el correo no coincide', status: false})
        }
    


    })

}

function actualizarDocente(req,res){
    var docenteId = req.params.id;
    var update = req.body;
    console.log(req.docente)
    if(docenteId != req.docente.sub){
       return res.status(500).send({message:'El docente no tiene permisos', status: false})
    }
    ModelDocente.findOneAndUpdate({_id: docenteId},update,{new:true},(err, docenteActualizado)=>{
        if(err) return res.status(500).send({message:'Error', status: false})

        res.status(200).send({docenteActualizado, status: true})

    })

}

function eliminarDocente(req,res){
   

}


module.exports = {
    home,
    insert,
    crearDocente,
    obtenerDocente,
    obtenerDocentes,
    login,
    actualizarDocente
}