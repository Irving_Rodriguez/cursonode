'use strict'
var mongoose = require('mongoose');
var configuracion = require('./configuracion/config');
var app = require('./app');
//npm install socket.io
var socketIO = require('socket.io')

mongoose.Promise = global.Promise;

mongoose.connect(configuracion.connexion)
.then(()=>{
    console.log('conexion a la base de datos exitosa');

    const server  = app.listen(configuracion.port,()=>{
        console.log('servidor corriendo exitosamente')
    })
    const io = socketIO.listen(server);
    io.on('connection',(socket)=>{
        console.log('new connection', socket.id)
        //reenviar a todos los usaurios incluyendome 
        socket.on('escuchaMensaje',(data)=>{
            // socket.to(data.username).emit('chat:message-server',data);
            io.sockets.emit('enviaMensaje', data)
        })
         //reenviar a  todos excepto a mi  
        socket.on('escuchaTyping', (data)=>{
            // console.log(data);
            // io.sockets.emit('chat:typing-serve', data)
            socket.broadcast.emit('enviaTyping', data)
    
        })
    
        io.sockets.emit('test event', 'here is some data')
    })
})
.catch(err =>console.log(err))

// ESCUCHAMOS CON EL EVENTO "ON"
// EVIAMONS CON EL EVENTO EMIT