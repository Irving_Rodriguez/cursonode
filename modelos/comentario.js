'use strict'
var moongose = require('mongoose');
var Schema = moongose.Schema;

var ComentariosSchema = new Schema({
    comentario:{type: String},
    tipo: {type: String, enum:['curso','docente']},
    emisor_estudiante: {type:Schema.ObjectId, ref: 'estudiantes'},
    receptor_docente: {type:Schema.ObjectId, ref: 'docentes'},
    receptor_curso: {type:Schema.ObjectId, ref: 'cursos'},
    calificacion: {type: Number, max: [10,'no existe en el rango']}

})

module.exports = moongose.model('comentarios', ComentariosSchema)