'use strict'
var moongose = require('mongoose');
var Schema = moongose.Schema;

var EtiquetasSchema = new Schema({
    etiqueta:{
         type:String,
         unique: true,
         lowercase: true,
         required:[true, 'etiqueta requerida']},
    referenciaId:[{type: String}]
})

module.exports = moongose.model('etiquetas', EtiquetasSchema)