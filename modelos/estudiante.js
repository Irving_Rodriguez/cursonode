'use strict'
var moongose = require('mongoose');
var Schema = moongose.Schema;

var validarCorreo = function(correo){
    var re = /^.+@.+$/
    return re.test(correo);
}

var EstudiantesSchema = new Schema({
    nombre: {type: String,
         minlength: 10,
         required: true,
         match:[/^[a-zA-Z ]+$/]
        },
    cargo: {type: String},
    correo:  {
        type: String,
        lowercase: true,
        validate: [validarCorreo, 'Este correo no es valido']
      },
      telefono:{type: String,
         match:[/\d{3}-\d{3}-\d{4}/]
        },
      conocimientos_previos: {type:String},
      curso: {type: Schema.ObjectId, ref:'cursos'},
      token_calificacion:{type: String},
      status_calificacion: {type: Boolean}
})

module.exports = moongose.model('estudiantes', EstudiantesSchema)