'use strict'

var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var CursoSchema = new Schema({
    nombre: {type: String, minlength: 4, required: true},
    temario_file: {type: String},
    temario:{type: Object, required: true},
    video_introduccion:{type:String},
    sumary:{type: String, required: true},
    docente:{type: Schema.ObjectId, ref: 'docentes'},
    fecha_inicio: Date,
    fecha_final: Date,
    status: {type: Number, enum:[1,2,3,4,5]},
    hora: {type: String, required: true},
    duracion: {type: Number, required: true}, //horas
    valoracion:{
        rating: {type: Number, default: 0},
        listRaiting: {type:[Number], default:[]}
    },
    habilidadesDesarrolladas: [String],
    precio: {type: Number, default: 0},
    cupoLimite: {type: Number, required: true},
    image: {type: String, default: null},
    registrados:[{type: Schema.ObjectId, ref: 'estudiantes'}],
    descripcion: {type: String, required: true},
    requisitos:[{type: String, default: 'No existen requisitos'}],
    ubicacion:String

})
//1- Inscripciones abiertas
//2- Activo
//3- cupo agotado
//4 - Finalizado
//5- pendiente
module.exports = mongoose.model('cursos',CursoSchema);