'use strict'
var moongose = require('mongoose');
var Schema = moongose.Schema;
var DocentesSchema = new Schema({
    nombre: String,
    cargo: String,
    resumen: {type:String, maxlength: [200,'Fuera de rango']},
    total_estudiantes: Number,
    imagen_perfil: String,
    correo:  {
        type: String,
        lowercase: true, // Always convert `test` to lowercase
        unique: true
      },
    password: String,
    redes_sociales: {
        facebook :{type:String, default: null},
        youtube :{type:String, default: null},
        twitter :{type:String, default: null},
        linkedin :{type:String, default: null}
    }    
})

module.exports = moongose.model('docentes', DocentesSchema);
